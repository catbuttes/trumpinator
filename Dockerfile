FROM debian:latest
LABEL language="python3"
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y install python3-pip

#create a non-root user. Why would you use root?
RUN groupadd -r pythonuser && useradd --no-log-init -g pythonuser pythonuser

#Set up dependancies
WORKDIR /home/pythonuser/code
COPY requirements.txt /home/pythonuser/code
RUN pip3 install -r requirements.txt

#Copy over the rest of the files and set up the ownership of it all
COPY Trumps/ /home/pythonuser/code/Trumps

COPY . /home/pythonuser/code
RUN chown -R pythonuser:pythonuser /home/pythonuser

USER pythonuser

EXPOSE 5000

ENV LC_ALL=C.UTF-8
ENV FLASK_APP app.py
ENTRYPOINT ["python3", "-m", "flask", "run", "--host=0.0.0.0"]
