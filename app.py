import flask
import random

trumpinator = flask.Flask(__name__)


@trumpinator.route('/')
def test():
    return "Wrong"

@trumpinator.route('/fail')
def trumpinate():
    filename = 'Trumps/{0}.gif'.format(random.randint(1,6))
    return flask.send_file(filename, mimetype='image/gif')

if __name__ == '__main__':
    trumpinator.run()
